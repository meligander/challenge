Challenge coded for apply digital.

Steps to run it:

- Open the terminal and go to the root project

- Install all needed dependencies: npm install

- Rename .env.example to .env: mv .env.example .env

- Change the connection string MONGO_URI=mongodb://(host):(port)/(database) in file .env (e.g. mongodb://localhost:27017/challenge)

- Run script dbDataRefresh to upload initial data (be sure to have mongodb running): npm run dbDataRefresh

- Run script to start application: npm start
