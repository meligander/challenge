const mongoose = require('mongoose');
const db = require('../config/db');
const data = require('./test.json');
const hackerNews = require('../utils/hackerNews');

require('dotenv').config();

db()
  .then(() => hackerNews(data))
  .then(() => console.log('Data refresh has been done'))
  .finally(() => mongoose.connection.close());
