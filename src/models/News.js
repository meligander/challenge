const mongoose = require('mongoose');

const NewsSchema = new mongoose.Schema({
  _id: {
    type: String,
    required: true,
  },
  author: {
    type: String,
  },
  created_at: {
    type: Date,
  },
  title: {
    type: String,
  },
  story_text: {
    type: String,
  },
  comment_text: {
    type: String,
  },
  story_title: {
    type: String,
  },
  story_url: {
    type: String,
  },
  _tags: [
    {
      type: String,
    },
  ],
  status: {
    type: String,
    enum: ['available', 'deleted'],
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

const News = mongoose.model('news', NewsSchema);

module.exports = News;
