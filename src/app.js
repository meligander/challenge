const express = require('express');
const cors = require('cors');

const app = express();

// Middleware
app.use(express.json({ limit: '50mb', extended: false }));
app.use(express.urlencoded({ limit: '50mb', extended: false }));
app.use(cors());
app.use('/api/news', require('./routes/api/news'));

module.exports = app;
