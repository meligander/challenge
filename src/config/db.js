const mongoose = require('mongoose');

const connectDB = async () => {
  return new Promise((resolve, reject) => {
    mongoose.connect(
      process.env.MONGO_URI,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      },
      (err) => {
        if (err) {
          console.error('Failed to connect to MongoDB');
          console.error(err);
          reject();
          process.exit(1);
        } else {
          console.log('MongoDB connected');
          resolve();
        }
      },
    );
  });
};

module.exports = connectDB;
