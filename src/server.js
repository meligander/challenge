const connectDB = require('./config/db');
const hackerNews = require('./utils/hackerNews');
const app = require('./app');

require('dotenv').config();

// Connect to the DB
connectDB()
  .then(() => hackerNews())
  .finally(() => console.log('Data collection has been perfomed'));

// Set an interval so it runs every hour
setInterval(
  () =>
    hackerNews().finally(() =>
      console.log('Data collection has been perfomed'),
    ),
  60 * 60 * 1000,
);

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server started on PORT: ${PORT}`));
