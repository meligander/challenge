const router = require('express').Router();
// Model
const News = require('../../models/News');

// @route    GET /api/news
// @desc     get all news
router.get('/', async (req, res) => {
  try {
    const { page = 1, author, _tags, title } = req.query;

    const filter = {
      status: 'available',
      ...(author && { author }),
      ...(title && {
        title: { $regex: `.*${title}.*`, $options: 'i' },
      }),
      ...(_tags && {
        _tags: { $all: _tags.split(',') },
      }),
    };

    const news = await News.find(filter)
      .limit(5)
      .skip((page - 1) * 5);

    if (news.length === 0) {
      return res.status(400).json({
        msg: "There isn't any match with these filters",
      });
    }

    return res.json(news);
  } catch (err) {
    console.error(err.message);
    return res.status(500).json({ msg: 'Server Error' });
  }
});

// @route    DELETE /api/news/:id
// @desc     It 'Deletes' a news
router.delete('/:id', async (req, res) => {
  try {
    const news = await News.findById(req.params.id);

    if (news && news.status === 'available') {
      news.status = 'deleted';
      await news.save();
    } else
      return res.status(400).json({
        msg: !news ? 'News does not exist' : 'News already deleted',
      });

    return res.json(news);
  } catch (err) {
    console.error(err.message);
    return res.status(500).json({ msg: 'Server Error' });
  }
});

module.exports = router;
