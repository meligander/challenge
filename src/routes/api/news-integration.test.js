const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');
const request = require('supertest');
const app = require('../../app');
const News = require('../../models/News');
const testData = require('../../data/test.json');

const mongoDB = new MongoMemoryServer();

// Connecting to the database before all tests.
beforeAll(async () => {
  await mongoDB.start();
  const uri = mongoDB.getUri();
  await mongoose.connect(uri);
});

// Initialize the db, before each test.
beforeEach(async () => {
  const results = testData.map((item) => {
    const newItem = new News(item);
    return newItem.save();
  });
  await Promise.all(results);
});

// Clear the db, remove all data after each test.
afterEach(async () => {
  const results = Object.values(mongoose.connection.collections).map(
    (collection) => collection.deleteMany(),
  );
  await Promise.all(results);
});

// Closing database connection all tests.
afterAll(async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
  await mongoDB.stop();
});

describe('Tests on API news', () => {
  describe('GET /api/news', () => {
    it('The response returns an array of available news', async () => {
      const res = await request(app).get('/api/news').send();

      expect(res.statusCode).toBe(200);
      expect(res.headers['content-type']).toContain('json');
      expect(res.body).toBeInstanceOf(Array);
      expect(res.body.every((item) => item.status === 'available')).toBe(true);
    });

    it('Pagination works and returns at least 5 elements', async () => {
      const res = await request(app).get('/api/news').send();

      expect(res.body).toBeInstanceOf(Array);
      expect(res.body.length).toBeLessThanOrEqual(5);
    });

    it('Filter with no match should return error', async () => {
      const response = await request(app).get('/api/news?title=kjx').send();

      expect(response.status).toBe(400);
      expect(response.body.msg).toBe(
        "There isn't any match with these filters",
      );
    });
  });

  describe('DELETE /api/news/:id', () => {
    const newsID = '33207991';

    afterEach(async () => {
      await News.findByIdAndUpdate(newsID, {
        $set: { status: 'available' },
      });
    });

    it('It correctly deletes a news', async () => {
      const res = await request(app).delete(`/api/news/${newsID}`);
      expect(res.statusCode).toBe(200);
      expect(res.headers['content-type']).toContain('json');

      const news = await News.findById(newsID);
      expect(news.status).toBe('deleted');
    });

    it('It does not find the news to delete', async () => {
      const res = await request(app).delete('/api/news/1');
      expect(res.statusCode).toBe(400);
      expect(res.body.msg).toBe('News does not exist');
    });

    it('It does not allowed to delete a news twice', async () => {
      const firstRes = await request(app).delete(`/api/news/${newsID}`);
      expect(firstRes.statusCode).toBe(200);

      const secondRes = await request(app).delete(`/api/news/${newsID}`);
      expect(secondRes.status).toBe(400);
      expect(secondRes.body.msg).toBe('News already deleted');
    });
  });
});
