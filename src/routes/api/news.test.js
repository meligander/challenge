const request = require('supertest');
const app = require('../../app');
const News = require('../../models/News');
const testData = require('../../data/test.json');

jest.mock('../../models/News');

describe('Tests on API news', () => {
  describe('GET /api/news', () => {
    const mockGetApi = (data) => {
      News.find.mockImplementation(() => ({
        limit: jest.fn().mockReturnThis(),
        skip: () => data || testData,
      }));
    };

    it('The response returns an array of available news', async () => {
      mockGetApi();
      const res = await request(app).get('/api/news').send();

      expect(res.statusCode).toBe(200);
      expect(res.headers['content-type']).toContain('json');
      expect(res.body).toBeInstanceOf(Array);
      expect(res.body.every((item) => item.status === 'available')).toBe(true);
    });

    it('Filter with no match should return error', async () => {
      mockGetApi([]);
      const response = await request(app).get('/api/news').send();

      expect(response.status).toBe(400);
      expect(response.body.msg).toBe(
        "There isn't any match with these filters",
      );
    });
  });

  describe('DELETE /api/news/:id', () => {
    const newsID = '33207991';
    const mockDeleteApi = (data, mockSave = jest.fn()) => {
      News.findById.mockImplementation((id) => {
        const [firstItem] = data.filter((item) => item._id === id);

        return firstItem ? { ...firstItem, save: mockSave } : null;
      });
    };

    it('It correctly deletes a news', async () => {
      const mockSave = jest.fn();
      mockDeleteApi([...testData], mockSave);

      const res = await request(app).delete(`/api/news/${newsID}`);
      expect(res.statusCode).toBe(200);
      expect(res.headers['content-type']).toContain('json');
      expect(mockSave).toHaveBeenCalledTimes(1);
    });

    it('It does not find the news to delete', async () => {
      mockDeleteApi([...testData]);

      const res = await request(app).delete('/api/news/1');
      expect(res.statusCode).toBe(400);
      expect(res.body.msg).toBe('News does not exist');
    });

    it('It does not allowed to delete an already deleted news', async () => {
      mockDeleteApi(
        [...testData].map((item) =>
          item._id === newsID ? { ...item, status: 'deleted' } : item,
        ),
      );

      const secondRes = await request(app).delete(`/api/news/${newsID}`);
      expect(secondRes.status).toBe(400);
      expect(secondRes.body.msg).toBe('News already deleted');
    });
  });
});
