const axios = require('axios');
const hackerNews = require('./hackerNews');
const News = require('../models/News');

jest.mock('axios');
jest.mock('../models/News');

describe('Tests on hackerNews', () => {
  it('', async () => {
    const newsID = '33207991';

    axios.get.mockImplementation(() =>
      Promise.resolve({
        data: {
          hits: [{ objectID: newsID }],
        },
      }),
    );

    const mockSave = jest.fn();
    News.mockImplementation(() => ({
      save: mockSave,
    }));

    await hackerNews();

    expect(News).toHaveBeenCalledTimes(1);
    expect(News).toHaveBeenCalledWith({
      _id: newsID,
      objectID: newsID,
      status: 'available',
    });
    expect(mockSave).toHaveBeenCalledTimes(1);
  });
});
