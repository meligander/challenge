const axios = require('axios');

const News = require('../models/News');

const saveItemIfNotFound = async (item) => {
  const news = await News.findById(item.objectID ? item.objectID : item._id);
  if (!news) {
    const newItem = new News({
      ...item,
      ...(item.objectID && { _id: item.objectID, status: 'available' }),
    });

    await newItem.save();
  }
};

const hackerNews = async (testData) => {
  try {
    const info = !testData
      ? await axios.get(
          'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
        )
      : {
          data: {
            hits: testData,
          },
        };

    const results = info.data.hits.map((item) => saveItemIfNotFound(item));
    return Promise.all(results);
  } catch (err) {
    console.log('Data collection coud not be performed');
    console.error(err.message);
    return Promise.reject();
  }
};

module.exports = hackerNews;
